import steamclient

users = steamclient.get_users()
user = users[0]
shortcuts = user.shortcuts
games = user.games()
libraries = steamclient.get_libraries() 



def test_user():
    print("User:", user)

def test_game():
    game = games[0]
    print("Game:", game)
    assert type(game.logo) is str
    assert type(game.grid) is str
    assert type(game.hero) is str



def test_shortcuts():
    shortcuts = user.shortcuts
    shortcut = shortcuts[0]
    print(shortcut)
    assert type(shortcut.entry_id) is int
    assert type(shortcut.id) is str
    assert type(shortcut.logo) is str
    assert type(shortcut.grid) is str
    assert type(shortcut.hero) is str