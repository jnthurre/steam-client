from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(name='steamclient',
      version='1.2',
      author='Avalon Parton',
      author_email='avalonlee@gmail.com',
      description='Steam Client API',
      long_description=long_description,
      long_description_content_type="text/markdown",
      license='MIT',
      url='https://gitlab.com/avalonparton/steam-client',
      packages=['steamclient'],
      install_requires=['requests'],
      zip_safe=False)
