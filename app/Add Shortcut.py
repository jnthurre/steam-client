"""
Drag an .exe onto this script to add it to Steam!
"""
import os
import random
import sys
import subprocess

import eel

import steamclient as steam

user = steam.get_users()[0]
path = None
name = None

if len(sys.argv) > 1:
    path = sys.argv[1]

    # Get name from basename before following links
    name = os.path.splitext(os.path.basename(path))[0]
    
    ## Follow all symlinks to find the real file
    #while os.path.islink(path):
    #    path = os.readlink(path

    # Windows - follow .lnk
    if path.strip().endswith(".lnk"):
        print(f"Following Windows Shortcut {path}")
        import pythoncom
        import win32com.client 

        shell = win32com.client.Dispatch("WScript.Shell")
        path = shell.CreateShortCut(path).Targetpath
        print(f"Found target: {path}")


print(f"Input path: {path}")


def process_exists(process_name):
    call = 'TASKLIST', '/FI', f'imagename eq {process_name}'
    # use buildin check_output right away
    output = subprocess.check_output(call)
    # check in last line for process name
    last_line = output.strip().split(b'\r\n')[-1]
    # because Fail message could be translated
    return last_line.lower().startswith(bytes(process_name.lower(), 'utf8'))

@eel.expose
def add_shortcut(name, target, start_dir, icon, launch_options, openvr, allow_overlay, tags):
    """ Returns a tuple of the following format: (result, steam_running)

        result: 0 if shortcut adding is success
                1 if shortcut with same name/exe exists
                2 if name/exe is empty
        steam_running: True if Steam.exe is running (to show Restart Steam button or not)
    """
    _tags = [tag.strip() for tag in tags.split(",")]
    s = user.add_shortcut(name=name,
                          exe=target,
                          start_dir=start_dir,
                          openvr=openvr,
                          icon=icon,
                          launch_options=launch_options,
                          allow_overlay=allow_overlay,
                          tags=_tags,
                          )
    print("Adding shortcut")
    print(f"name: {name}")
    print(f"exe : {target}")
    print(f"start_dir : {start_dir}")
    print(f"openvr : {openvr}")
    print(f"icon : {icon}")
    print(f"launch_options : {launch_options}")
    print(f"allow_overlay : {allow_overlay}")
    print(f"tags : {_tags}")
    return (s, process_exists("Steam.exe"))

@eel.expose
def get_file_info():
    info = {'launch_options': "", 'openvr': False, 'allow_overlay': True}
    info['name'] = name if name else ""
    info['target'] = f'"{path}"' if path else ""
    info['start_dir'] = f'"{os.path.dirname(path)}"' if path else ""
    info['icon'] = ""
    print("File info:", info)
    return info

@eel.expose
def restart_steam() -> bool:
    """ Attempts to restart steam, returns True on success """
    if process_exists("Steam.exe"):
        subprocess.call(['taskkill','/F','/IM','Steam.exe'])
        eel.sleep(2)

    return steam.start_client()



@eel.expose()
def stop():
    """ Exits """
    sys.exit(0)


eel.init('web')
port = random.randint(9000, 9500) # avoid running into self if multiple instances are running
eel.start('index.html', mode="chrome", port=port, size=(565, 685))
